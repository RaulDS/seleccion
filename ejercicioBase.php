<?php

require_once "header.php";
    insertHead("Crear cartas en formato texto | Página de la selección", "es", $tags = $DECLARACIONES_HEAD);
    insertNav($NAV);
    insertHeader("Crear cartas en formato texto | Página de la selección", "h1", "encabezado");
    
    // Comprobamos si los archivos se han generado correctamente y se lo hacemos saber al usuario
    if (crearCartas($jugadores, $paises, $TEMPLATE_CARTA)) {
        insertSimpleTag("Se ha creado/actualizado el contenido correctamente.", "p", "text-align-center");
    }
    else {
        insertSimpleTag("Ha habido un problema drante la creación.", "p", "text-align-center");
    }
require_once "footer.php";


?>