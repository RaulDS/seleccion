<?php
    // Archivo de funciones utilitarias generales



/* ------ ------------------------------------- ------ */
/* ------ Funciones para construir el documento ------ */
/* ------ ------------------------------------- ------ */
/*
    Función para iniciar la generación del documento HTML.
    Se utiliza en conjunto con la función insertFooter().

    Param @title(string): Título del documento.
        Default: "Documento sin ttítulo".
    Param @lang(string): Idioma del documento.
        Default: "es".
    Param @tags(array): Etiquetas que añadir en el head (scripts, css, links...).
        Estructura: Array de strings. Los strings son las etiquetas completas a añadir.
        Default: Array vacío.
    Return: Void.
*/
function insertHead(string $title = "Documento sin titulo", string $lang = "es", array $tags = []):void {
    // Convertimos los tags a añadir en un string único.
    $tagsString = "";
    for ($i = 0; $i < count($tags); $i++) {
        $tagsString = $tagsString.$tags[$i];
    }

    // Imprimimos el contenido con la plantilla y los valores proporiconados
    echo <<<HEAD
        <!DOCTYPE html>
        <html lang="$lang">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            $tagsString
            <title>$title</title>
        </head>
        <body>
    HEAD;
}

/*
    Función para acabar la generación del documento HTML.
    Se utiliza en conjunto con la función insertHead().

    Return: Void.
*/
function insertFooter():void {
    echo <<<HEAD
        </body>
        </html>
    HEAD;
}





/* ------ --------------------------------------------- ------ */
/* ------ Funciones para insertar componentes de diseño ------ */
/* ------ --------------------------------------------- ------ */
/*
    Función para insertar un navegador superior.

    Param @elementos(array): Array con los elementos del navegador.
        Estructura: Array multidimensional. Cada elemento hace referencia a un elemento del navegador.
            El primer subelemento al enlace y el segundo al contenido del enlace.
    Param @classes(string): Todas las clases que se le quieran añadir al contenedor del navegador.
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function insertNav(array $elementos, string $classes=""):void {
    echo '<nav id="main-nav" class="'.$classes.'"><ul>';
    for ($i = 0; $i < count($elementos); $i++) {
        $enlace = $elementos[$i][0];
        $content = $elementos[$i][1];

        // En caso de que la página actual aparezca en el navegador, destacamos dicho item
        if ($enlace == basename($_SERVER["PHP_SELF"])) {
            echo <<<ELEMENTO
                <li><a class="active" href="$enlace">$content</a></li>
            ELEMENTO;
        }
        else {
            echo <<<ELEMENTO
            <li><a href="$enlace">$content</a></li>
            ELEMENTO;
        }
    }
    echo "</ul></nav>";
}

/*
    Función para insertar un elemento de tipo header (h1, h2, h3, h4, h5 o h6)

    Param @title(string): Contenido del titular.
    Param @type(string): Tipo de titular [h1 | h2 | h3 | h4 | h5 | h6].
        Default: "h1".
    Param @classes(string): Todas las clases que se le quieran añadir al titular.
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function insertHeader(string $title, string $type="h1", string $classes=""):void {
    echo '<'.$type.' class="'.$classes.'">'.$title.'</'.$type.'>';
}





/* ------ --------------------------------- ------ */
/* ------ Funciones para insertar elementos ------ */
/* ------ --------------------------------- ------ */
/*
    Función para insertar un elemento cualquiera. Se puede utilizar para escribir contenido facilmente a través
    de etiquetas de parrafo, dado que es su valor por defecto.

    Param @content(string): Contenido a introducir dentro del elemento.
    Param @tagname(string): Etiqueta a insertar.
        Default: p.
    Param @classes(string): Todas las clases que se le quieran añadir al elemento.
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function insertSimpleTag(string $content, string $tagname="p", string $tagClasses=""):void {
    echo '<'.$tagname.' class="'.$tagClasses.'">'.$content.'</'.$tagname.'>';
}

/*
    Función para insertar una lista.

    Param @elementos(array): Array con los elementos de la lista.
        Estructura: Cada elemento del array es un elemento de la lista. En caso de querer
            crear un submenu, el primer elemento será un string con el titulo del submenu,
            y el segundo elemento será un array de dos elementos de dicha sublista.
    Param @tagname(string): Etiqueta a insertar.
        Default: p.
    Return: Void.
*/
function insertList(array $elementos, string $type="ul"):void {
    echo '<'.$type.'>';
    for ($i = 0; $i < count($elementos); $i++) {
        echo "<li>";

        // Detectamos si el elemento actual es array o string
        // Si es string lo imprimimos, en caso contrario el elemento es un submenu, hacemos llamada recursiva.
        if (gettype($elementos[$i]) == "string") {
            echo $elementos[$i];
        }
        else if (gettype($elementos[$i]) == "array") {
            echo $elementos[$i][0];
            array_shift($elementos[$i]);
            insertList($elementos[$i], $type);
        }
        echo "</li>";
    }
    echo '</'.$type.'>';
}

/*
    Función para iniciar un contenedor (div, section, article, main, aside...).

    Param @type(string): Etiqueta a insertar.
        Default: div.
    Param @classes(string): Todas las clases que se le quieran añadir al elemento.
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function startContainer(string $type="div", string $classes=""):void {
    echo '<'.$type.' class="'.$classes.'">';
}

/*
    Función para cerrar un contenedor (div, section, article, main, aside...).

    Param @type(string): Etiqueta a insertar.
        Default: div.
    Return: Void.
*/
function endContainer(string $type="div"):void {
    echo "</".$type.">";
}

/*
    Función para insertar una imagen.

    Param @src(string): Ruta a la imagen original.
    Param @src(alt): Texto alternativo de la imagen.
    Param @classes(string): Todas las clases que se le quieran añadir al elemento (se añaden
        al contenedor, no al elemento de imagen en concreto).
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function insertSingleImage(string $src, string $alt="", string $classes=""):void {
    echo '<div class="rds-image-container '.$classes.'"><img src="'.$src.'" alt="'.$alt.'"></div>';
}

/*
    Función para insertar un enlace.

    Param @content(string): Contenido del enlace (texto de enlace).
    Param @src(href): Direccción a la que apunta el enlace.
    Param @classes(string): Todas las clases que se le quieran añadir al elemento.
        Estructura: Todas las clases separadas por espacios.
        Default: String vacío.
    Return: Void.
*/
function insertLink(string $content, string $href, string $classes=""):void {
    echo '<a href="'.$href.'" class="'.$classes.'">'.$content.'</a>';
}