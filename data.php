<?php
/*
$players [
    playerName [
        imageRoute => imageName.extension (string)
        shirtNumber => number (int)
        position => position (string)
        nationality => isoCode (string)
        birthDate => date (string)
        goalScored => goals (int)
        gamesPlayed => games (int)
    ]
]
*/
$esPlayers = [
    //PORTERES
    "Misa Rodríguez" => [
        "imageRoute" => "misa.png",
        "shirtNumber" => 1,
        "position" => "Guardameta",
        "nationality" => "ESP",
        "birthDate" => "1999-07-22",
        "goalsScored" => 0,
        "gamesPlayed" => 3
    ],
    "Enith Salón" => [
        "imageRoute" => "enith_salon.png",
        "shirtNumber" => 13,
        "position" => "Guardameta",
        "nationality" => "ESP",
        "birthDate" => "2001-09-24",
        "goalsScored" => 0,
        "gamesPlayed" => 0
    ],
    "Cata Coll" => [
        "imageRoute" => "cata_coll.png",
        "shirtNumber" => 23,
        "position" => "Guardameta",
        "nationality" => "ESP",
        "birthDate" => "2001-04-23",
        "goalsScored" => 0,
        "gamesPlayed" => 4
    ],
    //DEFENSES
    "Ona Batlle" => [
        "imageRoute" => "ona_batlle.png",
        "shirtNumber" => 2,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "1999-06-10",
        "goalsScored" => 0,
        "gamesPlayed" => 7
    ],
    "Irene Paredes" => [
        "imageRoute" => "irene_paredes.png",
        "shirtNumber" => 4,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "1991-07-04",
        "goalsScored" => 0,
        "gamesPlayed" => 7
    ],
    "Ivana Andrés" => [
        "imageRoute" => "ivana_andres.png",
        "shirtNumber" => 5,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "1994-07-13",
        "goalsScored" => 0,
        "gamesPlayed" => 4
    ],
    "Oihane Hernández" => [
        "imageRoute" => "oihane_hernandez.png",
        "shirtNumber" => 12,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "2000-05-04",
        "goalsScored" => 0,
        "gamesPlayed" => 6
    ],
    "Laia Codina" => [
        "imageRoute" => "laia_codina.png",
        "shirtNumber" => 14,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "2000-01-22",
        "goalsScored" => 1,
        "gamesPlayed" => 4
    ],
    "Olga Carmona" => [
        "imageRoute" => "olga_carmona.png",
        "shirtNumber" => 19,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "2000-06-12",
        "goalsScored" => 2,
        "gamesPlayed" => 6
    ],
    "Rocío Gálvez" => [
        "imageRoute" => "rocio_galvez.png",
        "shirtNumber" => 20,
        "position" => "Defensa",
        "nationality" => "ESP",
        "birthDate" => "1997-04-15",
        "goalsScored" => 0,
        "gamesPlayed" => 1
    ],
    //MIGCAMPISTES
    "Teresa Abelleira" => [
        "imageRoute" => "teresa_abelleira.png",
        "shirtNumber" => 3,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "2000-01-09",
        "goalsScored" => 1,
        "gamesPlayed" => 7
    ],
    "Aitana Bonmatí" => [
        "imageRoute" => "aitana.png",
        "shirtNumber" => 6,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "1998-01-18",
        "goalsScored" => 3,
        "gamesPlayed" => 7
    ],
    "Irene Guerrero" => [
        "imageRoute" => "irene_guerrero.png",
        "shirtNumber" => 7,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "1996-12-12",
        "goalsScored" => 0,
        "gamesPlayed" => 3
    ],
    "Alexia Putellas" => [
        "imageRoute" => "alexia.png",
        "shirtNumber" => 11,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "1994-02-04",
        "goalsScored" => 0,
        "gamesPlayed" => 7
    ],
    "María Pérez" => [
        "imageRoute" => "maria_perez.png",
        "shirtNumber" => 16,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "2001-12-24",
        "goalsScored" => 0,
        "gamesPlayed" => 1
    ],
    "Claudia Zornoza" => [
        "imageRoute" => "claudia_zornoza.png",
        "shirtNumber" => 21,
        "position" => "Centrocampista",
        "nationality" => "ESP",
        "birthDate" => "1990-10-20",
        "goalsScored" => 0,
        "gamesPlayed" => 2
    ],
    //DAVANTERES
    "Mariona" => [
        "imageRoute" => "mariona_caldentey.png",
        "shirtNumber" => 8,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "1996-03-19",
        "goalsScored" => 1,
        "gamesPlayed" => 6
    ],
    "Esther González" => [
        "imageRoute" => "esther_gonzalez.png",
        "shirtNumber" => 9,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "1992-12-08",
        "goalsScored" => 1,
        "gamesPlayed" => 5
    ],
    "Jenni Hermoso" => [
        "imageRoute" => "jenni_hermoso.png",
        "shirtNumber" => 10,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "1990-05-09",
        "goalsScored" => 3,
        "gamesPlayed" => 7
    ],
    "Eva Navarro" => [
        "imageRoute" => "eva_navarro.png",
        "shirtNumber" => 15,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "2001-01-27",
        "goalsScored" => 0,
        "gamesPlayed" => 5
    ],
    "Alba Redondo" => [
        "imageRoute" => "alba_redondo.png",
        "shirtNumber" => 17,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "1996-08-27",
        "goalsScored" => 3,
        "gamesPlayed" => 7
    ],
    "Salma Paralluelo" => [
        "imageRoute" => "salma_paralluelo.png",
        "shirtNumber" => 18,
        "position" => "Delantera",
        "nationality" => "ESP",
        "birthDate" => "2003-11-13",
        "goalsScored" => 2,
        "gamesPlayed" => 7
    ]
];



$paises = [
    "ES" => [
        "nombre" => "España",
        "mote" => "La Roja",
        "jugadoresTotales" => count($esPlayers),
        "nombreImagenBandera" => "ES.png",
        "mundiales" => 1,
        "eurocopas" => 3
    ]
];

?>