<?php
    // Archivo con declaraciones iniciales y custom functions

    // Añadimos el contenido del archivo de funciones y de base de datos.
    require_once "functions.php";
    require_once "data.php";

    // Declaramos el navegador global de las páginas
    $NAV = [
        ["index.php","Home"],
        ["ejercicio1.php","Generar cartas TXT"],
        ["ejercicio2.php","Mostrar cartas TXT"],
        ["ejercicio3.php","Generar y mostrar cartas HTML"],
        ["ejercicio4.php","Mostrar cartas HTML"]
    ];

    $NAV_COACH = [
        ["index.php","Home"],
        ["ejercicio1.php","Generar cartas TXT"],
        ["ejercicio2.php","Mostrar cartas TXT"],
        ["ejercicio3.php","Generar y mostrar cartas HTML"],
        ["ejercicio4.php","Mostrar cartas HTML"],
        ["ejercicio5.php","Ejercicio entrenadores 1"]
    ];

    // Declaramos las etiquetas que queremos añadir al HEAD del documento
    $DECLARACIONES_HEAD = [
        '<link rel="stylesheet" href="assets/css/main.css">'
    ];

    // Declaramos el template de las cartas
    $TEMPLATE_CARTA = <<<CARTA
        Dear [name],
        Congratulations! You has been selected to be part of the [country] national football team. 
        I wish you the best!
    CARTA;


    

    

    // Funciones concretas de este ejercicio


    /*
        Esta función imprime un conjunto de jugadores

        Param @data(array): Array de jugadores.
        Param @data_paises(array): Array de paises.
        Return: Void.
    */
    function imprimimrJugadores($data, $data_paises):void {
        startContainer("section", "tabla-jugadores");
            // Recorremos el array de jugadores
            foreach ($data as $jugador => $datos) {
                startContainer("article");
                    startContainer("div", "encabezadoJugador");
                        insertSimpleTag($datos["dorsal"], "strong");
                        insertHeader($jugador, "h3");
                        insertSimpleTag($datos["posicion"], "strong");
                    endContainer();
                    startContainer("div", "imagenPerfil");
                        insertSingleImage("assets/img/banderas/".$data_paises[$datos["pais"]]["nombreImagenBandera"], "", "bandera");
                        insertSingleImage("assets/img/jugadores/".$datos["nombreDeImagen"]);
                    endContainer();
                    insertSimpleTag("<strong>Fecha de nacimiento:</strong> ".$datos["fechaDeNacimiento"]);
                    insertSimpleTag("<strong>Goles marcados:</strong> ".$datos["númeroDeGolesMarcados"]);
                    insertSimpleTag("<strong>Partidos jugados:</strong> ".$datos["númeroDePartidosJugados"]);
                endContainer("article");
            }
        endContainer("section");
    }

    /*
        Esta función genera las cartas a los jugadores proporcionados.

        Param @jugadores(array): Array de jugadores.
        Param @paises(array): Array de paises.
        Param @template(string): Plantilla de la carta.
        Param @method(string): Acciñon rincipal de la función.
            Options: [save | print]. Se guarda en una dirección por defecto o se printa por pantalla.
        Param @contentType(string): Tipo de contenido de la carta.
            Options: [text | HTML]. Formato texto o formato HTML.
            Default: null.
        Return(bool): Devuelve true en caso de terminar correctamente.
    */
    function crearCartas($jugadores, $paises, $template, $method="save", $contentType=null):bool {
        if ($contentType != null && $contentType == "HTML") {
            // Documento con indice de enlaces a las cartas en formato HTML (Ejercicio 4)
            $contentIndex = file_get_contents("cartas/templates/index.indice.header.html");
        }
        
        foreach ($jugadores as $nombre => $datos) {
            $pais = $paises[$datos["pais"]]["nombre"];
            if ($method == "save") {
                $nombre = strtr($nombre, " ", "_");
                if ($contentType != null && $contentType == "HTML") {
                    $contentIndex = $contentIndex.'<li><a href="cartas/archivosHtml/'.$nombre.'.html">Carta a '.$nombre.'</a></li>';
                    file_put_contents("cartas/archivosHtml/".$nombre.".html", strtr($template, [
                        "[name]" => $nombre,
                        "[country]" => $paises[$datos["pais"]]["nombre"]
                    ]));
                }
                else {
                    file_put_contents("cartas/archivosTexto/".$nombre.".txt", strtr($template, [
                        "[name]" => $nombre,
                        "[country]" => $paises[$datos["pais"]]["nombre"]
                    ]));
                }
            }
            else if ($method == "print") {
                insertHeader("Carta para ".$nombre, "h3");
                $printContent = strtr($template, [
                    "[name]" => $nombre,
                    "[country]" => $paises[$datos["pais"]]["nombre"]
                ]);
                if ($contentType == null || $contentType != "HTML") {
                    $printContent = "<pre>".$printContent."</pre><br>";
                }
                insertSimpleTag($printContent);
            }
        }
        if ($contentType != null && $contentType == "HTML") {
            $contentIndex = $contentIndex.file_get_contents("cartas/templates/index.indice.footer.html");
            file_put_contents("cartasJugadores.html", $contentIndex);
        }
        return true;
    }